# Exercice_1



## Description (EN)


The goal of this exercice is to create the set of class which allow to manage car location agency.
You can get a Java and C# version in different folder :
                * Java
                * C#
- All unit test are in  RentalTest class.
- Read the documentation of class and variable name for understand the program.

## Description (FR)

Le but de cet exercice est de creer un ensemble de classes permettant de gerer une agence de location de voitures.
Vous pouvez avoir une version en Java et une autre en C# dans les differents dossiers : 
                * Java
                * C#
- Les tests JUnit de cet exercice sont dans la classe RentalTest.
- Lire attentivement la documentation, le nom des variables et des classes pour mieux comprendre le programme.

