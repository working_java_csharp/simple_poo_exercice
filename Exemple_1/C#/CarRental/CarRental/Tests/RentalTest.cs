﻿using CarRental.Model;
using CarRental.Manager;
using System.Collections.Generic;
using System.Reflection.Emit;
using Xunit;

namespace CarRental.Tests
{
    public class RentalTest
    {
       [Fact]
        public void newCar()
        {
           Car car = new Car("ford mustang", 2014);
           Assert.Equal("ford mustang", car.Model);
        }
        
            
        [Fact]
        public void newCarNoModel()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => new Car(null, 2014));
            Assert.Equal(ex.Message, "");
        }
     
        [Fact]
        public void carRentalAddRemove()
        {
           CarRentalManager rental = new CarRentalManager();
           rental.add(new Car("ford mustang", 2013));
           rental.remove(new Car("ford mustang", 2013));
           Assert.Equal("", rental.ToString());
        }

        [Fact]
        public void carRentalRemoveNotExist()
        {
            CarRentalManager rental = new CarRentalManager();
           var ex = Assert.Throws<InvalidOperationException>(() => rental.remove(new Car("ford mustang", 2013)));
           Assert.Equal(ex.Message, "");
        }

        [Fact]
        public void caRentalAddNull()
        {
            CarRentalManager rental = new CarRentalManager();
            var ex = Assert.Throws<ArgumentNullException>(() => rental.add(null));
            Assert.Equal(ex.Message, "");
        }

        [Fact]
        public void carRentaltoString()
        {
            CarRentalManager rental = new CarRentalManager();
           rental.add(new Car("audi tt", 2001));
           rental.add(new Car("ford mustang", 2006));
            Assert.Equal("audi tt 2001\nford mustang 2006", rental.ToString());
         }

        [Fact]
        public void carRentaltoSell()
        {
            int currentYear = DateTime.UtcNow.Year;
            CarRentalManager rental = new CarRentalManager();
            rental.add(new Car("ford mustang", currentYear - 3));
            var toSell = rental.toSell();
            Assert.True(toSell.Contains(new Car("ford mustang", currentYear - 3)));
        }

        [Fact]
        public void carRentaltoSellEmpty()
        {
            int currentYear = DateTime.UtcNow.Year;
            CarRentalManager rental = new CarRentalManager();
            rental.add(new Car("audi tt", currentYear));
            rental.add(new Car("ford mustang", currentYear - 1));
            var toSell = rental.toSell();
            Assert.True(!toSell.Any());
        }
        [Fact]
        public void newCamel()
        {
            Camel camel = new Camel(2014);
            Assert.Equal(camel, new Camel(2014));
        }
        [Fact]
        public void camelToString()
        {
            Camel camel = new Camel(2014);
            Assert.Equal("camel 2014", camel.ToString());
        }

        [Fact]
        public void carRentalCarAndCamel()
        {
            CarRentalManager rental = new CarRentalManager();
            rental.add(new Car("ford mustang", 2014));
            rental.add(new Camel(2010));
            rental.remove(new Camel(2010));
            rental.remove(new Car("ford mustang", 2014));
         }

        [Fact]
        public void carRentalCarAndCamel2()
        {
            CarRentalManager rental = new CarRentalManager();
            var ex = Assert.Throws<ArgumentNullException>(() => rental.remove(new Camel(2010)));
            Assert.Equal(ex.Message, "");
        }
        [Fact]
        public void carRentalCarAndCamelToSell()
        {
            int currentYear = DateTime.UtcNow.Year;
            CarRentalManager rental = new CarRentalManager();
            rental.add(new Car("ford mustang", currentYear - 10));
            rental.add(new Camel(currentYear - 10));
            var toSell = rental.toSell();
            Assert.True(toSell.Contains(new Car("ford mustang", currentYear - 10)));
            Assert.True(toSell.Contains(new Camel(currentYear - 10)));
        }

        [Fact]
        public void carRentalCarAndCamelToSell2()
        {
            int currentYear = DateTime.UtcNow.Year;
            CarRentalManager rental = new CarRentalManager();
            rental.add(new Car("ford mustang", currentYear - 3));
            rental.add(new Camel(currentYear - 3));
            var toSell = rental.toSell();
            Assert.True(toSell.Contains(new Car("ford mustang", currentYear - 3)));
            Assert.False(toSell.Contains(new Camel(currentYear - 3)));
        }

        [Fact]
        public void findACarByModel()
        {
            CarRentalManager rental = new CarRentalManager();
            rental.add(new Car("ford mustang", 2020));
            rental.add(new Camel(2003));
            Assert.Equal(new Car("ford mustang", 2020), rental.findACarByModel("ford mustang").Value);
        }

        [Fact]
        public void findACarByModel2()
        {
            CarRentalManager rental = new CarRentalManager();
            rental.add(new Car("renault alpine", 1992));
            rental.add(new Camel(1992));
            Assert.False(rental.findACarByModel("ford mustang").HasValue);
        }

    }
}
