﻿namespace CarRental.Model
{
    public class Car : Rental
    {
        private readonly string model;
        private readonly int sellYear = 2;
        public string Model { get { return model; } }
        public Car(string _model, int year) : base(year)
        {
            this.model = _model ?? throw new ArgumentNullException("model can not be null");
        }

        public override bool canSell()=> DateTime.UtcNow.Year - Year >= sellYear;

        public override string ToString()=>model +" "+Year;

        public override bool Equals(object? o)
        {
            if (this == o) return true;
            if (o == null ||  !(o is Car)) return false;
            Car? car = o as Car;
            return Year == car?.Year && model.Equals(car.model);
        }

        public override int GetHashCode()
        {
            return model.GetHashCode()^Year.GetHashCode();
        }


    }
}
