﻿using System.Reflection;

namespace CarRental.Model
{
    public class Camel : Rental
    {
        private readonly int sellYear = 5;
        public Camel(int year) : base(year)
        {
        }

        public override bool canSell() => DateTime.UtcNow.Year - Year >= sellYear;

        public override string ToString() => nameof(Camel)+" " + Year;

        public override bool Equals(object? o)
        {
            if (this == o) return true;
            if (o == null || !(o is Camel)) return false;
            Camel? car = o as Camel;
            return Year == car?.Year;
        }

        public override int GetHashCode()
        {
            return Year.GetHashCode();
        }
    }
}
