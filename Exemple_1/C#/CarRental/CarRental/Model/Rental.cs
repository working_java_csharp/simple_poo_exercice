﻿using CarRental.interfaces;

namespace CarRental.Model
{
    public abstract class Rental : IRental
    {
        private readonly int year;

        public Rental(int year)
        {
            if (year < 0) throw new Exception("Year can not be null");
            this.year = year;
        }
        public int Year { get { return year; } }

        public abstract bool canSell();
       
    }
}
