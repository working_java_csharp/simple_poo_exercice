﻿using CarRental.interfaces;
using CarRental.Model;
using Microsoft.CodeAnalysis;

namespace CarRental.Manager
{
    public class CarRentalManager : ICarRental
    {
        private readonly List<IRental> rentals;

        public CarRentalManager() => rentals = new List<IRental>();
        public void add(IRental rental)
        {
            if (rental == null) throw new ArgumentNullException("Add value can not be null");
            rentals.Add(rental);    
        }

        public Optional<Car> findACarByModel(string model)
        {
            var resul = rentals.FirstOrDefault(x => x is Car && ((Car)x).Model == model);
            if (resul == null) return new Optional<Car>();
            return new Optional<Car>((Car)resul);
        }

        public void remove(IRental rental)
        {
            if (!rentals.Remove(rental))throw new InvalidOperationException("Value not exist");
        }

        public List<IRental> toSell()=>rentals.Where(x => x.canSell()).ToList();
        

        public override string ToString()=> String.Join("\n", rentals.Select(x => x.ToString()));
            
        
    }
}
