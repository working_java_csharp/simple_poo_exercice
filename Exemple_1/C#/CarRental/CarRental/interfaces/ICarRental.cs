﻿using CarRental.Model;
using Microsoft.CodeAnalysis;

namespace CarRental.interfaces
{
    public interface ICarRental
    {
        void add(IRental rental);
        void remove(IRental rental);
        Optional<Car> findACarByModel(String model);
        List<IRental> toSell();
    }
}
