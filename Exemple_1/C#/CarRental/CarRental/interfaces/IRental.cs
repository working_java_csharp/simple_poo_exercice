﻿using System;

namespace CarRental.interfaces
{
    public interface IRental
    {
        bool canSell();
    }
}
