package klaze.rental.manager;

import klaze.rental.interfaces.ICarRental;
import klaze.rental.interfaces.IRental;
import klaze.rental.model.Car;
import klaze.rental.model.Rental;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class CarRental implements ICarRental {
    private final List<IRental> rentals;

    public CarRental(){
        rentals = new ArrayList<>();
    }

    public void add(IRental rental){
        rentals.add(Objects.requireNonNull(rental));
    }

    public void remove(IRental rental){
        if(!rentals.remove(rental)){
            throw new IllegalStateException();
        }
    }

    public Optional<Car> findACarByModel(String model){
        var result = rentals.stream().filter(rental -> rental instanceof Car && ((Car) rental).getModel().equals(model))
                .findFirst();
        return result.isEmpty() ? Optional.empty()
                                : Optional.of((Car)result.get());
    }

    public List<IRental> toSell(){
        return rentals.stream().filter(IRental::canSell).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return rentals.stream().map(IRental::toString).collect(Collectors.joining("\n"));
    }
}
