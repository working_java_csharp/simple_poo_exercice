package klaze.rental.model;

import java.time.LocalDate;
import java.util.Objects;

public class Car extends Rental {
    private final String model;
    private final int sellYear = 2;
    public Car(String name, int year){
        super(year);
        this.model = Objects.requireNonNull(name);
    }

    public String getModel() {
        return model;
    }


    @Override
    public boolean canSell() {
        return LocalDate.now().getYear() - getYear() >= this.sellYear;
    }

    public int getYear() {
        return super.getYear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return super.getYear() == car.getYear() && model.equals(car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, super.getYear());
    }

    @Override
    public String toString() {
        return  model + ' ' + super.getYear() ;
    }
}
