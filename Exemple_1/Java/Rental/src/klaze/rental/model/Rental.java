package klaze.rental.model;

import klaze.rental.interfaces.IRental;

public abstract class Rental implements IRental {
    private final int year;

    public Rental(int year){
        if(year <= 0) throw new IllegalArgumentException("Year ==> "+year);
        this.year = year;
    }

    public abstract boolean canSell();
    public int getYear() {
        return year;
    }
}
