package klaze.rental.model;

import java.time.LocalDate;
import java.util.Objects;

public class Camel extends Rental{
    private final int sellYear = 5;
    public Camel(int year) {
        super(year);
    }

    @Override
    public boolean canSell() {
       return LocalDate.now().getYear() - getYear() >= this.sellYear;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Camel car = (Camel) o;
        return super.getYear() == car.getYear();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getYear());
    }

    @Override
    public String toString() {
        return "camel "+super.getYear();
    }
}
