package klaze.rental.interfaces;

public interface IRental {
    boolean canSell();
}
