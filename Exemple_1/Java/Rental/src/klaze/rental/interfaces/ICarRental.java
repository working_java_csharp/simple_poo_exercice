package klaze.rental.interfaces;

import klaze.rental.model.Car;
import klaze.rental.model.Rental;

import java.util.List;
import java.util.Optional;

public interface ICarRental {
    void add(IRental rental);
    void remove(IRental rental);
    Optional<Car> findACarByModel(String model);
    List<IRental> toSell();
}
